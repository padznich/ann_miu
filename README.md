# ANN based on Rumelhart Perceptron

## Accuracy: 0.93

## The model
We're using a simple [perceptron](https://www.cs.cmu.edu/afs/cs.cmu.edu/academic/class/15381-f01/www/handouts/110601.pdf), built with **Tensorflow.**

- Input: [ 9 nurons ]
- Hidden 1: [ 9 nurons ] FC (Fully Connected)
- Output: [ 2 nurons ]

+ Weight initialization: RandomNormal
+ Activation:-
  + Sigmoid

* 20 epochs of 10,000 iterations each


## Input
9 nodes

> Touple in the input data set obviously match this format


Output:
 + 0-1 rate, for isAttackPacket === true.
 + 0-1 rate, for isLegalkPacket === true.

## About the files
Contain about 1 million UDP packets.

> Please use Notepad++ or Sublime to view the data set on Windows

- raw_data_...txt: Attack and normal datasets taken from the internet
- IPTrain_data_...txt: Data and label for training the model
- test_data_...txt: Data and label for testing the trained model
- MODEL_TEST: Saved model