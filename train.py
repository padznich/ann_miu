import os

import tensorflow as tf
import numpy as np
from plotly.offline import plot
import plotly.graph_objs as ob


raw_data = open("IPtrain_data_udp.txt"). readlines()
raw_labels = open("IPtrain_label_udp.txt"). readlines()
trained_path = './model_test'


x = []
y = []
gra = []
gro = []

for _ in raw_data:
    _ = _.split()
    x.append([int(_[0]), int(_[1]), int(_[2]), int(_[3])])


for _ in raw_labels:
    y.append([0, 1])


def get_next_batch(batch_size):

    _data = raw_data[get_next_batch.counter: get_next_batch.counter + batch_size]
    _label = raw_labels[get_next_batch.counter: get_next_batch.counter + batch_size]

    get_next_batch.counter += batch_size

    batch_data = []
    batch_label = []

    for _ in _data:
        batch_data.append(_.split())

    for _ in _label:
        batch_label.append(_.split())

    return np.array(batch_data), np.array(batch_label)


nodesForLayerInput = 9
nodesForLayer1 = 9
nodesForLayerOut = 2


data = tf.placeholder('float', shape=[None, nodesForLayerInput])
label = tf.placeholder('float')

layer1 = {
    'w': tf.Variable(tf.random_normal([nodesForLayerInput, nodesForLayer1])),
    'b': tf.Variable(tf.random_normal([nodesForLayer1]))
}

layerOut = {
    'w': tf.Variable(tf.random_normal([nodesForLayer1, nodesForLayerOut])),
    'b': tf.Variable(tf.random_normal([nodesForLayerOut]))
}

saver = tf.train.Saver()


def graph(_data):
    ans_layer1 = tf.nn.relu(tf.add(tf.matmul(_data, layer1['w']), layer1['b']))

    ans_layer_out = tf.add(tf.matmul(ans_layer1, layerOut['w']), layerOut['b'])

    return ans_layer_out


def train(_x):
    prediction = graph(_x)

    cost = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(_sentinel=None,
                                                                  logits=prediction,
                                                                  labels=label,
                                                                  dim=-1,
                                                                  name=None))

    optimiser = tf.train.AdamOptimizer().minimize(cost)

    n_epochs = 20

    with tf.Session() as sess:
        sess.run(tf.global_variables_initializer())

        for epoch in range(n_epochs):
            epoch_loss = 0
            get_next_batch.counter = 0

            for i in range(10000):
                epoch_data, epoch_label = get_next_batch(100)
                i, c = sess.run([optimiser, cost], feed_dict={data: epoch_data, label: epoch_label})
                epoch_loss += c

            print("Training Batch: of Epoch: " + str(epoch + 1) + "\tLoss: " + str(epoch_loss))

            gra.append(epoch_loss)

        save_path = saver.save(sess, os.path.join(trained_path, "model_train.ckpt"))

        print("Saved to: ", save_path)

        correct = tf.equal(tf.argmax(prediction, 1), tf.argmax(label, 1))
        accuracy = tf.reduce_mean(tf.cast(correct, 'float'))

        print("Accuracy ", accuracy.eval({data: x, label: y}))


train(data)
