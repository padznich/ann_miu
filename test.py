import tensorflow as tf


raw_data = open("./data_sets/test_data_udp.txt").readlines()
raw_labels = open("./data_sets/test_label_udp.txt").readlines()

x = []
y = []

for _ in raw_data:
    _ = _.split()
    x.append([int(_[0]), int(_[1]), int(_[2]), int(_[3])])

for _ in raw_labels:
    y.append([0, 1])

nodesForLayerInput = 9
nodesForLayer1 = 9

numberOfClassesOut = 2

data = tf.placeholder('float', shape=[None, 4])
label = tf.placeholder('float')

layer1 = {
    'w': tf.Variable(tf.zeros([nodesForLayerInput, nodesForLayer1])),
    'b': tf.Variable(tf.zeros([nodesForLayer1]))
}

layerOut = {
    'w': tf.Variable(tf.zeros([nodesForLayer1, numberOfClassesOut])),
    'b': tf.Variable(tf.zeros([numberOfClassesOut]))
}

saver = tf.train.Saver()


def graph(_data):
    ans_layer1 = tf.nn.relu(tf.add(tf.matmul(_data, layer1['w']), layer1['b']))

    ans_layer_out = tf.add(tf.matmul(ans_layer1, layerOut['w']), layerOut['b'])
    return ans_layer_out


def train(_x):
    prediction = graph(_x)

    cost = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(_sentinel=None,
                                                                  logits=prediction,
                                                                  labels=label,
                                                                  dim=-1,
                                                                  name=None))
    optimiser = tf.train.AdamOptimizer().minimize(cost)

    n_epochs = 1

    with tf.Session() as sess:

        sess.run(tf.global_variables_initializer())

        saver.restore(sess, "./model_test/model_train.ckpt")

        for epoch in range(n_epochs):

            epoch_loss = 0

            for i in range(100):
                i, c = sess.run([optimiser, cost], feed_dict={data: x, label: y})

                epoch_loss += c
                print(c)

        correct = tf.equal(tf.argmax(prediction, 1), tf.argmax(label, 1))
        accuracy = tf.reduce_mean(tf.cast(correct, 'float'))
        print("Accuracy ", accuracy.eval({data: x, label: y}))


train(data)
